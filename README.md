# DEPRECATED: use terraform-aws-modules/acm/aws

# Module my_certificate

## Usage

### terraform >= 0.13

```
module "backends" {

  source = "git::ssh://git@bitbucket.org/asolidodev/my_certificate.git?ref=1.0.0"

  for_each = toset(var.certificates)

  my_certificate_domain_name = each.key
  my_certificate_zone_id     = data.aws_route53_zone.main.zone_id

}
```

### terraform <= 0.12

```
module "backends" {

  source = "git::ssh://git@bitbucket.org/asolidodev/my_certificate.git?ref=0.0.1"

  my_certificate_create      = var.cert_create
  my_certificate_domain_name = "*.${var.domain_name}"
  my_certificate_zone_id     = data.aws_route53_zone.main.zone_id

}
```

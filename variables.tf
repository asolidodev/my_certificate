# Remember to pass a Virgina provider if you need the certificate to be in this region
variable "my_certificate_domain_name" {
  type        = string
  description = "The domain name to be used for the certificate (e.g. 'example.com', '*.example.com', '*.api.example.com')"
}
variable "my_certificate_zone_id" {
  type        = string
  description = "The ID of the zone where the record to validate the certificate is to be created. In theory you could parse the domain name with splits and joins to use a data to get this. But one hand that's fragile and on the other hand if you use a data on the root module then you can reuse it in any module declaration (instead of several data in each module)."
}

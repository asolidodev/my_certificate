# The certificate
resource "aws_acm_certificate" "cert" {
  domain_name       = var.my_certificate_domain_name
  validation_method = "DNS"
  tags = {
    ManagedByTerraform = "true"
    TerraformWorkspace = "${terraform.workspace}"
  }
  # We follow the documentation of the TF docs for this resource:
  # "It's recommended to specify create_before_destroy = true in a lifecycle block to replace a certificate which is currently in use (eg, by aws_lb_listener)."
  lifecycle {
    create_before_destroy = true
  }
}

# We follow the example of aws_acm_certificate_validation for the following two resources
resource "aws_route53_record" "cert_validation" {

  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  name    = each.value.name
  type    = each.value.type
  zone_id = var.my_certificate_zone_id
  records = [each.value.record]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "cert" {

  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [aws_route53_record.cert_validation[each.key].fqdn]
}
